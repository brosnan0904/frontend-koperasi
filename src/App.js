import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import './App.css';
import CreateAnggota from "./component/anggota/CreateAnggota";
import EditAnggota from "./component/anggota/EditAnggota";
import ListAnggotaComponent from "./component/anggota/ListAnggotaComponent";
import LoginAnggota from "./component/anggota/LoginAnggota";

class App extends Component {
  render() {
    return (
        <Router>
          <div className="container">
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
              <Link to={'/'} className="navbar-brand">HOME</Link>
              <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav mr-auto">
                  <li className="nav-item">
                    <Link to={'/create'} className="nav-link">Create</Link>
                  </li>
                  <li className="nav-item">
                    <Link to={'/index'} className="nav-link">Index</Link>
                  </li>
                </ul>
              </div>
            </nav> <br/>
            <h2>Aplikasi Sistem Informasi Koperasi</h2> <br/>
            <Switch>
              <Route exact path='/' component={ LoginAnggota } />
              <Route exact path='/create' component={ CreateAnggota } />
              <Route path='/edit/:id' component={ EditAnggota } />
                <Route path='/index' component={ ListAnggotaComponent } />
            </Switch>
          </div>
        </Router>
    );
  }


}

export default App;
